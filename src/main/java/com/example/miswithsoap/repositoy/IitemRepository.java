package com.example.miswithsoap.repositoy;

import com.example.miswithsoap.bean.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IitemRepository extends JpaRepository<Item,Integer> {
}
