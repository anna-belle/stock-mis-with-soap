package com.example.miswithsoap.repositoy;

import com.example.miswithsoap.bean.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IsupplierRepository extends JpaRepository<Supplier, Integer> {
}
