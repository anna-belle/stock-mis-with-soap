package com.example.miswithsoap.bean;

import com.mis.stock.item.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Item {

    @Id
    @GeneratedValue

    private int id;
    private String name;
    private String itemCode;
    private Status status;
    private int price;
    @OneToOne
    private Supplier supplier;

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", itemCode='" + itemCode + '\'' +
                ", status=" + status +
                ", price=" + price +
                ", supplier=" + supplier +
                '}';
    }
}
