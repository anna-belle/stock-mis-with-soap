package com.example.miswithsoap.endpoint;

import com.example.miswithsoap.bean.Item;
import com.example.miswithsoap.bean.Supplier;
import com.example.miswithsoap.repositoy.IitemRepository;
import com.example.miswithsoap.repositoy.IsupplierRepository;
import com.mis.stock.item.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
public class itemEndpoint {
    @Autowired
    private IitemRepository iItemRepository;
    @Autowired
    private IsupplierRepository supplierRepository;
    @PayloadRoot(namespace = "http://stock.mis.com/item",localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemResponse findItem(@RequestPayload GetItemRequest request)
    {
        Item item= iItemRepository.findById(request.getId()).get();
        GetItemResponse itemDetailsResponse = mapItemDetails(item);
        return itemDetailsResponse;
    }
    @PayloadRoot(namespace = "http://stock.mis.com/item",localPart = "GetAllItemDetailsRequest")
    @ResponsePayload
    public GetAllItemsResponse getAllItems(@RequestPayload GetAllItemsRequest request)
    {
        GetAllItemsResponse itemResp = new GetAllItemsResponse();
        System.out.println("Reached here");
        List<Item> items = iItemRepository.findAll();
        System.out.println("List: "+ items);
        for(Item item: items){
            GetItemResponse itemDetailsResponse = mapItemDetails(item);
            itemResp.getItem().add(itemDetailsResponse.getItem());
        }
        return  itemResp;
    }
    @PayloadRoot(namespace = "http://stock.mis.com/item",localPart = "CreateItemDetailsRequest")
    @ResponsePayload
    public CreateItemResponse saveItem(@RequestPayload CreateItemRequest request) {
        Supplier supplier = supplierRepository.findById(request.getItem().getSupplier()).get();
        Item testItem = iItemRepository.save(new Item(
                request.getItem().getItemId(),
                request.getItem().getName(),
                request.getItem().getItemCode(),
                request.getItem().getStatus(),
                request.getItem().getPrice(),
                supplier
        ));
        System.out.println("Test: "+testItem);
//        System.out.println("course details "+ course);
        CreateItemResponse itemDetailsResponse = new CreateItemResponse();
        itemDetailsResponse.setItem(request.getItem());
        itemDetailsResponse.setMessage("Created Successfully");
        return itemDetailsResponse;
    }
    @PayloadRoot(namespace = "http://stock.mis.com/item",localPart = "DeleteItemDetailsRequest")
    @ResponsePayload
    public DeleteItemResponse deleteitem(@RequestPayload DeleteItemRequest request) {
        System.out.println("ID: "+request.getId());
        iItemRepository.deleteById(request.getId());
        DeleteItemResponse itemDetailsResponse = new DeleteItemResponse();
        itemDetailsResponse.setMessage("Deleted Successfully");
        return itemDetailsResponse;
    }
    private UpdateItemResponse mapUpdateItemResponse(Item item, String message) {
        ItemDetails itemDetails = mapItem(item);
        UpdateItemResponse resp = new UpdateItemResponse();
        resp.setItem(itemDetails);
        resp.setMessage(message);
        return resp;
    }
    private GetItemResponse mapItemDetails(Item item){
        ItemDetails details = mapItem(item);
        GetItemResponse itemDetailsResponse = new GetItemResponse();
        itemDetailsResponse.setItem(details);
        return itemDetailsResponse;
    }
    private ItemDetails mapItem(Item item){
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setItemCode(item.getItemCode());
        itemDetails.setItemId(item.getId());
        itemDetails.setName(item.getName());
        itemDetails.setPrice(item.getPrice());
        itemDetails.setStatus(item.getStatus());
        itemDetails.setSupplier(item.getSupplier().getId());
        return itemDetails;
    }
}
