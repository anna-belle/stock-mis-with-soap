package com.example.miswithsoap.endpoint;

import com.example.miswithsoap.bean.Supplier;
import com.example.miswithsoap.repositoy.IsupplierRepository;
import com.mis.stock.supplier.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class supplierEndpoint {

    @Autowired
    private IsupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://stock.mis.com/supplier", localPart = "getSupplierRequest")
    @ResponsePayload
    public GetSupplierResponse findById(@RequestPayload GetSupplierRequest request) {

        Supplier supplier = supplierRepository.findById(request.getId()).get();

        GetSupplierResponse supplierResponse = mapSupplierDetails(supplier);
        return  supplierResponse;
    }

    @PayloadRoot(namespace = "http://stock.mis.com/supplier", localPart = "GetAllSuppliersRequest")
    @ResponsePayload
    public GetAllSuppliersResponse findAll(@RequestPayload GetAllSuppliersRequest request){
        GetAllSuppliersResponse allCourseDetailsResponse = new GetAllSuppliersResponse();

        List<Supplier> suppliers = supplierRepository.findAll();
        for (Supplier supplier: suppliers){
            GetSupplierResponse supplierResponse = mapSupplierDetails(supplier);
            allCourseDetailsResponse.getSupplier().add(supplierResponse.getSupplier());
        }
        return allCourseDetailsResponse;
    }


    @PayloadRoot(namespace = "http://stock.mis.com/supplier", localPart = "CreateCourseDetailsRequest")
    @ResponsePayload
    public CreateSupplierResponse save(@RequestPayload CreateSupplierRequest request) {
        supplierRepository.save(new Supplier(request.getSupplier().getSupplierId(),
                request.getSupplier().getNames(),
                request.getSupplier().getEmail(),
                request.getSupplier().getMobile()
        ));

        CreateSupplierResponse supplierResponse = new CreateSupplierResponse();
        supplierResponse.setSupplier(request.getSupplier());
        supplierResponse.setMessage("Created Successfully");
        return supplierResponse;
    }

    @PayloadRoot(namespace = "http://stock.mis.com/supplier", localPart = "UpdateCourseDetailsRequest")
    @ResponsePayload
    public UpdateSupplierResponse update(@RequestPayload UpdateSupplierRequest request) {
        UpdateSupplierResponse supplierDetailResponse = null;
        Optional<Supplier> existingSupplier = this.supplierRepository.findById(request.getSupplier().getSupplierId());
        if(existingSupplier.isEmpty() || existingSupplier == null) {
            supplierDetailResponse = mapSupplierDetail(null, "Id not found");
        }
        if(existingSupplier.isPresent()) {

            Supplier _supplier = existingSupplier.get();
            _supplier.setNames(request.getSupplier().getNames());
            _supplier.setEmail(request.getSupplier().getEmail());
            _supplier.setMobile(request.getSupplier().getMobile());
            supplierRepository.save(_supplier);
            supplierDetailResponse = mapSupplierDetail(_supplier, "Updated successfully");

        }
        return supplierDetailResponse;
    }

    @PayloadRoot(namespace = "http://stock.mis.com/supplier", localPart = "DeleteCourseDetailsRequest")
    @ResponsePayload
    public DeleteSupplierResponse save(@RequestPayload DeleteSupplierRequest request) {

        System.out.println("ID: "+request.getId());
        supplierRepository.deleteById(request.getId());

        DeleteSupplierResponse supplierDetailsResponse = new DeleteSupplierResponse();
        supplierDetailsResponse.setMessage("Deleted Successfully");
        return supplierDetailsResponse;
    }

    private GetSupplierResponse mapSupplierDetails(Supplier supplier){
        SupplierDetails courseDetails = mapSupplier(supplier);

        GetSupplierResponse courseDetailsResponse = new GetSupplierResponse();

        courseDetailsResponse.setSupplier(courseDetails);
        return courseDetailsResponse;
    }

    private UpdateSupplierResponse mapSupplierDetail(Supplier supplier, String message) {
        SupplierDetails courseDetails = mapSupplier(supplier);
        UpdateSupplierResponse courseDetailsResponse = new UpdateSupplierResponse();

        courseDetailsResponse.setSupplier(courseDetails);
        courseDetailsResponse.setMessage(message);
        return courseDetailsResponse;
    }

    private SupplierDetails mapSupplier(Supplier supplier){
        SupplierDetails supplierDetails = new SupplierDetails();
        supplierDetails.setEmail(supplier.getEmail());
        supplierDetails.setSupplierId(supplier.getId());
        supplierDetails.setNames(supplier.getNames());
        return supplierDetails;
    }
}
