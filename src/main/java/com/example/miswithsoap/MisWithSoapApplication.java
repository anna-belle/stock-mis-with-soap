package com.example.miswithsoap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MisWithSoapApplication {

    public static void main(String[] args) {
        SpringApplication.run(MisWithSoapApplication.class, args);
    }

}
