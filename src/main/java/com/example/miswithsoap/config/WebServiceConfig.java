package com.example.miswithsoap.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
public class WebServiceConfig {
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(messageDispatcherServlet, "/ws/annabelle/stocks/*");
    }
    @Bean(name = "supplier")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema supplierSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("supplierPort");
        definition.setTargetNamespace("http://stock.mis.com/supplier");
        definition.setLocationUri("/ws/annabelle/stocks");
        definition.setSchema(supplierSchema);
        return definition;

    }


    @Bean(name = "item")
    public DefaultWsdl11Definition Wsdl11Definition(XsdSchema itemSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("itemPort");
        definition.setTargetNamespace("http://stock.mis.com/item");
        definition.setLocationUri("/ws/annabelle/stocks");
        definition.setSchema(itemSchema);
        return definition;
    }
    @Bean
    public XsdSchema supplierSchema() {
        return new SimpleXsdSchema(new ClassPathResource("supplier.xsd"));
    }
    public XsdSchema itemSchema() {
        return new SimpleXsdSchema(new ClassPathResource("item.xsd"));
    }
}
