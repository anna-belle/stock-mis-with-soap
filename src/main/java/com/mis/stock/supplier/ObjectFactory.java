//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.06.25 at 05:19:51 pm CAT 
//


package com.mis.stock.supplier;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mis.stock.supplier package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mis.stock.supplier
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSupplierRequest }
     * 
     */
    public GetSupplierRequest createGetSupplierRequest() {
        return new GetSupplierRequest();
    }

    /**
     * Create an instance of {@link CreateSupplierResponse }
     * 
     */
    public CreateSupplierResponse createCreateSupplierResponse() {
        return new CreateSupplierResponse();
    }

    /**
     * Create an instance of {@link SupplierDetails }
     * 
     */
    public SupplierDetails createSupplierDetails() {
        return new SupplierDetails();
    }

    /**
     * Create an instance of {@link UpdateSupplierRequest }
     * 
     */
    public UpdateSupplierRequest createUpdateSupplierRequest() {
        return new UpdateSupplierRequest();
    }

    /**
     * Create an instance of {@link UpdateSupplierResponse }
     * 
     */
    public UpdateSupplierResponse createUpdateSupplierResponse() {
        return new UpdateSupplierResponse();
    }

    /**
     * Create an instance of {@link CreateSupplierRequest }
     * 
     */
    public CreateSupplierRequest createCreateSupplierRequest() {
        return new CreateSupplierRequest();
    }

    /**
     * Create an instance of {@link GetAllSuppliersResponse }
     * 
     */
    public GetAllSuppliersResponse createGetAllSuppliersResponse() {
        return new GetAllSuppliersResponse();
    }

    /**
     * Create an instance of {@link DeleteSupplierRequest }
     * 
     */
    public DeleteSupplierRequest createDeleteSupplierRequest() {
        return new DeleteSupplierRequest();
    }

    /**
     * Create an instance of {@link GetSupplierResponse }
     * 
     */
    public GetSupplierResponse createGetSupplierResponse() {
        return new GetSupplierResponse();
    }

    /**
     * Create an instance of {@link GetAllSuppliersRequest }
     * 
     */
    public GetAllSuppliersRequest createGetAllSuppliersRequest() {
        return new GetAllSuppliersRequest();
    }

    /**
     * Create an instance of {@link DeleteSupplierResponse }
     * 
     */
    public DeleteSupplierResponse createDeleteSupplierResponse() {
        return new DeleteSupplierResponse();
    }

}
